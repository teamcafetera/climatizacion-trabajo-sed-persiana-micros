# SISTEMA DE PERSIANAS JAVI, JAIME, BRET

Este sistema dispone de dos modalidades: manual y automática.
El ususario podrá controlar el modo mediante un interruptor

1.  MODO MANUAL:
El usario dispone de un interruptor para elegir si quiere las persianas subidas o bajadas.
2.  MODO AUTOMÁTICO: 
La habitación dispone de 3 sensores:
    *  Sensor de presencia: situado dentro de la casa (detectará la presencia de personas dentro de la habitación).
	*  Sensor de humedad: situado fuera de la casa (detectará la presencia de lluvia/niebla en el exterior).
	*  Sensor de luminosidad: fuera de la casa (detectará la presencia o no de sol).


Este sistema de manejo automático de persianas es a gusto del cliente.
El cliente nos ha pedido las siguientes condiciones:

1. Siempre que llueva las persianas se bajarán para evitar manchar las ventanas.
2. Siempre que no haya luz suficiente para iluminar la casa (sea de noche) las persianas se bajarán.
3. Siempre que no haya presencia en la habitación las persianas estarán bajadas.

![Tabla FuncionamientoSistema](images/tabla_persianas.png)

Para considerar un sensor activo deberán trascurrir 10 segundos desde la aparición del evento (para que un simple rayo de luz
no suba las persianas, o una pequeña gota de agua las baje)

## MAQUETA

![Maqueta_Sistema](images/Maqueta.jpeg)

## DISTRIBUCIÓN EN LA PLACA

![Distribucion_Pines_Placa](images/DistribucionPlaca.png)



## VÍDEO FUNCIONAMIENTO PERSIANA

[Persiana manual y automatica](https://youtu.be/KsggwYbUNKY )
