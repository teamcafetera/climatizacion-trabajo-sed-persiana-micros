/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define Echo_Pin GPIO_PIN_0
#define Echo_GPIO_Port GPIOA
#define Echo_EXTI_IRQn EXTI0_IRQn
#define Final_de_Carrera_Arriba_Pin GPIO_PIN_1
#define Final_de_Carrera_Arriba_GPIO_Port GPIOA
#define Final_De_Carrerra_Abajo_Pin GPIO_PIN_2
#define Final_De_Carrerra_Abajo_GPIO_Port GPIOA
#define Lluvia_Pin GPIO_PIN_3
#define Lluvia_GPIO_Port GPIOA
#define LDR_Pin GPIO_PIN_4
#define LDR_GPIO_Port GPIOA
#define Trigger_Pin GPIO_PIN_5
#define Trigger_GPIO_Port GPIOA
#define Lectura_Se_ales_Pin GPIO_PIN_7
#define Lectura_Se_ales_GPIO_Port GPIOA
#define B_Manual_Pin GPIO_PIN_10
#define B_Manual_GPIO_Port GPIOB
#define B_automatico_Pin GPIO_PIN_12
#define B_automatico_GPIO_Port GPIOB
#define B_Reposo_Pin GPIO_PIN_13
#define B_Reposo_GPIO_Port GPIOB
#define B_subir_Pin GPIO_PIN_14
#define B_subir_GPIO_Port GPIOB
#define B_Bajar_Pin GPIO_PIN_15
#define B_Bajar_GPIO_Port GPIOB
#define B_subirD8_Pin GPIO_PIN_8
#define B_subirD8_GPIO_Port GPIOD
#define Led_Manual_Pin GPIO_PIN_12
#define Led_Manual_GPIO_Port GPIOD
#define Led_Auxiliar_Pin GPIO_PIN_13
#define Led_Auxiliar_GPIO_Port GPIOD
#define Led_Automatico_Pin GPIO_PIN_14
#define Led_Automatico_GPIO_Port GPIOD
#define Led_Reposo_Pin GPIO_PIN_15
#define Led_Reposo_GPIO_Port GPIOD
#define Motor_1_4_Pin GPIO_PIN_10
#define Motor_1_4_GPIO_Port GPIOA
#define Motor_2_4_Pin GPIO_PIN_13
#define Motor_2_4_GPIO_Port GPIOA
#define Motor_3_4_Pin GPIO_PIN_14
#define Motor_3_4_GPIO_Port GPIOA
#define Motor_4_4_Pin GPIO_PIN_15
#define Motor_4_4_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
