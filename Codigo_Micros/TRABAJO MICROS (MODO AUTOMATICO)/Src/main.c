/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

TIM_HandleTypeDef htim4;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM4_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
volatile int Estado;
typedef enum{REPOSO,MANUAL,AUTOMATICO}tipoestado;
uint8_t ADC_Val;
#define Val_Max_LDR 130
#define Val_Max_Distancia 10
volatile int tiempo;
volatile float distancia;
const float VelSon = 34000.0;

	int lectura_ultrasonido(void)
	{
		//Ponemos el trigger en estado bajo y esperamos 2 ms
	HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,0);
	HAL_Delay(2);
	//Ponemos el trriger en estado alto y esperamos 10ms
	HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,1);
	HAL_Delay(10);
	//Comenzamos poniendo el trigger en estado bajo
	HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,0);
	distancia = tiempo * 0.000001 * VelSon / 2.0;
	HAL_Delay(300);
	if(distancia<Val_Max_Distancia)return 1;
	if(distancia>Val_Max_Distancia)return 0;
  } 
			void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)//Detecta la salida del trigger en el pin 0 cuando se manda un pulso
{
	  tiempo=0;                       
		while(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0))//el tiempo cuenta mientras no detecte el trigger de vuelta
		tiempo++;
	
}
	
	int lectura_lluvia(void)
	{
		if(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_3)==1)return 1;//Lectura del sensor de lluvia de un bit
		else return 0;
	}
	
	int lectura_LDR(void)
	{
		HAL_ADC_Start(&hadc1);
		if (HAL_ADC_PollForConversion(&hadc1,10000000)==HAL_OK)
			{
				ADC_Val=HAL_ADC_GetValue(&hadc1);
			}
			HAL_ADC_Stop(&hadc1);
			return ADC_Val;//Lectura del LDR con el conversor anal�gico digital
	}
	
	int lectura_sensores(void)
	{
			if(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_7)==0)return 2; //PWM de periodo 10s a ciclo de trabajo 10% permite la lectura de sensores durante 1 s cada 10 s
		if((lectura_lluvia()) && (lectura_LDR() > Val_Max_LDR) && (lectura_ultrasonido()))
			return 1; //Se cumplen las condiciones para que la persiana suba
		else                      
			return 0;//No se cumplen las condiciones entonces la persiana baja
	}


	void subir_manual(void)
{

	while(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1)==0 && HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_14)==1)
	//Cuando se detecta el bot�n que sube la persiana y no se detecta el fin de carrera superior
  //El motor empieza a subir la persiana
	{

			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_RESET);
			
			HAL_Delay(2);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_RESET);
			
			HAL_Delay(2);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_RESET);
			
			HAL_Delay(2);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_SET);
			
			HAL_Delay(2);
		}
	//HAL_GPIO_WritePin(GPIOD,GPIO_PIN_13,0);
}
void bajar_manual(void)
{
	while(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_2)==0 && HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_15)==1)
		//Cuando se detecta el bot�n que baja la persiana y no se detecta el fin de carrera inferior
    //El motor empieza a bajar la persiana
	{
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_SET);
			
			HAL_Delay(2);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_RESET);
			
			HAL_Delay(2);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_RESET);
			
			HAL_Delay(2);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_RESET);
			
			HAL_Delay(2);
		}
}

	void subir(void)
{
	while(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1)==0 && HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_13)==0 && HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_10)==0)
	{
		//bajada autom�tica cuando no est� pulsado el fin de carrera de abajo, ni el boton de reposo o autom�tico
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_RESET);
			
			HAL_Delay(2);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_RESET);
			
			HAL_Delay(2);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_RESET);
			
			HAL_Delay(2);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_SET);
			
			HAL_Delay(2);
		}
}
void bajar(void)
{
	while(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_2)==0 && HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_13)==0 && HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_10)==0)
	{// Subida autom�tica cuando no est� pulsado el fin de carrera de abajo, ni el boton de reposo o autom�tico
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_SET);
			
			HAL_Delay(2);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_RESET);
			
			HAL_Delay(2);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_RESET);
			
			HAL_Delay(2);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_10,GPIO_PIN_SET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_13,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_14,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15,GPIO_PIN_RESET);
			
			HAL_Delay(2);
		}
}
	void Manual (void){
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,1);//Se enciende el led verde cuando estamos en modo manual 
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_14,0);
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_15,0);
		while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_13)==0 && HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_12)==0)
		{
			if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_14)==1)subir_manual();
			if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_15)==1)bajar_manual();
		}
	}
	
	
	
	
	void Automatico(void){
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,0);
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_14,0);
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_15,1); //Se enciende el led azul cuando estamos en el modo automatico
		while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_13)==0 && HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_10) == 0)
		{
			if(lectura_sensores()==1){bajar();}
			if(lectura_sensores()==0){subir();}
      if(lectura_sensores()==2)continue;
	
		}
	}
	
	
	void Reposo(void){
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,0);
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_14,1); //Se enciende el led rojo cuando estamos en el modo reposo
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_15,0);
	//	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_13,1);
	}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_TIM4_Init();
  /* USER CODE BEGIN 2 */
	Estado=REPOSO;
	Reposo();
	HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_1); 
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
		if (HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_10) && (Estado==REPOSO || Estado==AUTOMATICO))// Pasamos al modo manual cuando se pulsa el bot�n 
			{                                                                       //asignado al pin 10 y estamos en el estado reposo o autom�tico
			Estado=MANUAL;
			Manual();
		}
		if (HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_12) && (Estado==REPOSO || Estado==MANUAL))//Pasamos a modo autom�tico
			{
			Estado=AUTOMATICO;
			Automatico();
		}
		if (HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_13) && ( Estado==MANUAL || Estado==AUTOMATICO ))//Pasamos a modo reposo
			{
			Estado=REPOSO;
			Reposo();
		}
//			if(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_7)==0)HAL_GPIO_WritePin(GPIOD,GPIO_PIN_13,0);
//				if(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_7)==1)HAL_GPIO_WritePin(GPIOD,GPIO_PIN_13,1);
			
	}
		  
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_8B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 16000;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 10000;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 1000;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */
  HAL_TIM_MspPostInit(&htim4);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, Trigger_Pin|Motor_1_4_Pin|Motor_2_4_Pin|Motor_3_4_Pin 
                          |Motor_4_4_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, Led_Manual_Pin|Led_Auxiliar_Pin|Led_Automatico_Pin|Led_Reposo_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : Echo_Pin */
  GPIO_InitStruct.Pin = Echo_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(Echo_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : Final_de_Carrera_Arriba_Pin Final_De_Carrerra_Abajo_Pin Lluvia_Pin Lectura_Se_ales_Pin */
  GPIO_InitStruct.Pin = Final_de_Carrera_Arriba_Pin|Final_De_Carrerra_Abajo_Pin|Lluvia_Pin|Lectura_Se_ales_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : Trigger_Pin Motor_1_4_Pin Motor_2_4_Pin Motor_3_4_Pin 
                           Motor_4_4_Pin */
  GPIO_InitStruct.Pin = Trigger_Pin|Motor_1_4_Pin|Motor_2_4_Pin|Motor_3_4_Pin 
                          |Motor_4_4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : B_Manual_Pin B_automatico_Pin B_Reposo_Pin B_subir_Pin 
                           B_Bajar_Pin */
  GPIO_InitStruct.Pin = B_Manual_Pin|B_automatico_Pin|B_Reposo_Pin|B_subir_Pin 
                          |B_Bajar_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : B_subirD8_Pin */
  GPIO_InitStruct.Pin = B_subirD8_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(B_subirD8_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : Led_Manual_Pin Led_Auxiliar_Pin Led_Automatico_Pin Led_Reposo_Pin */
  GPIO_InitStruct.Pin = Led_Manual_Pin|Led_Auxiliar_Pin|Led_Automatico_Pin|Led_Reposo_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
